from os import path
import pygame

music_commands = {
    "stop": "Leállította a zenét!",
    "play": "Elindította a zenét!",
    "pause": "Megállította a zenét!",
    "unpause": "Folytatta a zenét!",
    "vol +": "Növelte a hangerőt!",
    "vol -": "Csökkentette a hangerőt!"
}


def stop_music():
    pygame.mixer.music.stop()


def pause_music():
    pygame.mixer.music.pause()


def unpause_music():
    pygame.mixer.music.unpause()


def play_music():
    if path.exists('music//Casino_music.mp3'):
        pygame.mixer.music.load("music//Casino_music.mp3")
        pygame.mixer.music.play(loops=-1)
    else:
        print("\t\tNem található a zene fájl!")


def volume_Increase():
    actual_volume = pygame.mixer.music.get_volume()
    if actual_volume == 1:
        return
    else:
        pygame.mixer.music.set_volume(actual_volume + 0.2)


def volume_Decrease():
    actual_volume = pygame.mixer.music.get_volume()
    if actual_volume <= 0.2:
        return
    else:
        pygame.mixer.music.set_volume(actual_volume - 0.2)


def is_playing_music():
    return pygame.mixer.music.get_busy()


def set_volume(x):
    pygame.mixer.init()
    pygame.mixer.music.set_volume(x / 100)


def actual_volume_level():
    actual_level = pygame.mixer.music.get_volume()
    return int(actual_level * 100)


music_operations = {
    "stop": lambda: stop_music(),
    "pause": lambda: pause_music(),
    "unpause": lambda: unpause_music(),
    "play": lambda: play_music(),
    "vol +": lambda: volume_Increase(),
    "vol -": lambda: volume_Decrease(),
}

# Can't use in dictionary the parenthesis for the function as it will run all of them without calling!
def musichandling(a):
    if a not in music_commands:
        return
    elif a in music_commands:
        feedback4user = music_commands[a]
        print("\t\t" + feedback4user)
        music_operations[a]()
        # Important note here is the parenthesis alias '()'
        # withouth the '()' the dictionary only calling the the function, not preform it!!!!
