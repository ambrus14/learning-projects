import copy
import enum
import random
import sys
import time
# import abs --- abstract base class

import config
import statistics
from cards import *


# Reminder make private for class variables if not used outside the class
# Reminder make new variable in case of used more than once

class MainMenuOptions(enum.Enum):
    NEW_GAME_WITH_INIT = 1
    LOAD_GAME_MENU = 2
    CONFIG_SETTINGS = 3
    STATISTICS = 4
    QUIT_FUNCTION = 5


class RoundStartingUserOptions(enum.Enum):
    START_ROUND = 1
    MODIFY_STARTING_BET = 2
    SAVE_GAME = 3
    EXIT_GAME = 4


class Player:
    def __init__(self):
        self.name: str = ""
        self.player_cards: typing.List[Card] = list()
        self.bank: float = 0.0
        self.bet: float = 0.0
        self.split_cards: typing.List[Card] = list()  # in case of splitting this variable used for the second hand
        self.statistics = statistics.statistics()  # statistics class példányosítása

    def print_cards_nicely(self, is_split_cards=False):
        cards_list = []
        if not is_split_cards:
            cards_list = copy.deepcopy(self.player_cards)
        elif is_split_cards:
            cards_list = copy.deepcopy(self.split_cards)
        nu_cards_to_print = len(cards_list)
        card_representation_list: list = []
        for i in range(0, nu_cards_to_print):
            card_representation_list.append(f'{cards_list[i].representation}'.split('\n'))
        card_line = [''] * 6
        for i in range(0, nu_cards_to_print):
            for j in range(0, 6):
                card_line[j] += card_representation_list[i][j] + ' '
        for i in range(0, len(card_line)):
            print(card_line[i])
        card_line.clear()
        card_representation_list.clear()
        cards_list.clear()
        return


class Game:
    def __init__(self):
        self.mixed: typing.List[Card] = list()  # Reminder: nem kell példányosítani csak hivatkozni az osztály névre
        self.Roasted_Cards: typing[Card] = list()  # make private variables if not used outside the class the variable
        self.Player = Player()
        self.Dealer = Player()
        self.deck = Deck()
        self.game_config = config.Config()  # config.py class példányosítása
        self.__Dealer_first_card_repr: str = ""
        self.__insurance_bet: float = 0.0
        self.__doubling_decision: bool = False
        self.__insurance_decision: bool = False
        self.__insurance_offer: bool = False
        self.__splitting_decision: bool = False
        self.__splitting_offer: bool = False  # Needed to use to avoid multiple splitting hands

    def check_blackjack(self, checked_cards=None, isPlayer=False) -> bool:
        if checked_cards is None:
            checked_cards = []
        if checked_cards[0].name == "A" and checked_cards[1].value == 10:
            if isPlayer:
                self.Player.statistics.nu_of_blackjack += 1
            return True
        elif checked_cards[1].name == "A" and checked_cards[0].value == 10:
            if isPlayer:
                self.Player.statistics.nu_of_blackjack += 1
            return True
        else:
            return False

    @staticmethod
    def sum_card_values(cards_list=None):
        if cards_list is None:
            cards_list = []
        len_of_card_list = len(cards_list)
        value_of_cards = 0
        for i in range(0, len_of_card_list):
            value_of_cards += cards_list[i].value
        return value_of_cards

    def initialise_new_game(self):
        #  setting up game
        self.Player.name = self.game_config.player_name
        self.Player.bank = self.game_config.starting_money
        self.Player.bet = self.game_config.starting_bet
        self.Dealer.name = "Osztó"
        self.Dealer.bank = self.game_config.starting_money
        self.Dealer.bet = self.game_config.starting_bet
        self.Roasted_Cards.clear()
        self.Dealer.player_cards.clear()
        self.Player.player_cards.clear()
        self.Player.split_cards.clear()
        self.shuffle_five_decks()
        print("\tÚj játékot kezdett!")
        return

    def dealer_turn(self):
        # dealer draws cards till sum of cards are 16
        dealer_cards = self.Dealer.player_cards
        dealer_cards[0].representation = self.__Dealer_first_card_repr
        self.Dealer.print_cards_nicely(is_split_cards=False)
        print("Az osztó lapjainak értéke: {}".format(self.sum_card_values(dealer_cards)))
        while self.sum_card_values(dealer_cards) < 16:
            dealer_cards.append(self.mixed.pop(-1))
            self.Dealer.print_cards_nicely(is_split_cards=False)
            print("Az osztó lapjainak értéke: {}".format(self.sum_card_values(dealer_cards)))
            time.sleep(1)
        return

    def exit_game(self):
        self.Player.statistics.save_stats()
        if self.save_game():
            print("\tMentette az aktuális játék állást!")
        else:
            print("\tNem mentette a játék állást, visszatérés a főmenübe!")
        self.mixed.clear()
        self.Roasted_Cards.clear()
        self.Player.player_cards.clear()
        self.Dealer.player_cards.clear()
        self.Player.split_cards.clear()
        return

    def mixed_to_json(self) -> list:
        nu_of_mixed_cards = len(self.mixed)
        mixed_dict: list = []
        for i in range(0, nu_of_mixed_cards):
            mixed_dict.append(self.mixed[i].color + self.mixed[i].name, )
        return mixed_dict

    def roasted_to_json(self) -> list:
        nu_of_roasted_cards = len(self.Roasted_Cards)
        roasted_dict: list = []
        for i in range(0, nu_of_roasted_cards):
            roasted_dict.append(self.Roasted_Cards[i].color + self.Roasted_Cards[i].name, )
        return roasted_dict

    def to_json(self):
        return {
            "Player": {
                "Player bank": self.Player.bank,
                "Player bet": self.Player.bet
            },
            "Dealer": {
                "Dealer name": self.Dealer.name,
                "Dealer bank": self.Dealer.bank,
                "Dealer bet": self.Dealer.bet
            },
            "Mixed deck": self.mixed_to_json(),
            "Roasted cards": self.roasted_to_json(),
        }

    @staticmethod
    def list_saving_users(game_list=None):
        if game_list is None:
            game_list = []
        list_of_saving_users = []
        for i in range(0, len(game_list)):
            list_of_saving_users.append(game_list[i]["Player name"])
        return list_of_saving_users

    @staticmethod
    def load_games() -> list:
        while True:
            try:
                with open("gamesaves.json", "r", encoding="UTF-8") as save_file:
                    saved_games = json.load(save_file)
                return saved_games
            except FileNotFoundError:
                print("\tA játékok mentésére szolgáló fájl nem létezett! Most létre jött!")
                with open("gamesaves.json", "w", encoding="UTF-8"):
                    pass
                continue

    def users_saved_games_name(self, game_list=None) -> list:
        if game_list is None:
            game_list = []
        saved_games_name_list = []
        for i in range(0, len(game_list)):
            if self.Player.name in self.list_saving_users(game_list)[i]:
                saved_games_name_list.append(game_list[i]["Game name"])
                continue
        return saved_games_name_list

    def print_users_saved_games(self, game_list=None) -> bool:
        if game_list is None:
            game_list: typing.List = []
        print("\tAz {} felhasználónévhez tartozó mentések:".format(self.Player.name))
        users_saved_games_name_list = self.users_saved_games_name(game_list)
        if not users_saved_games_name_list:
            print("\tAz Ön felhasználó nevéhez nem tartozik mentés!")
            return False
        elif users_saved_games_name_list:
            for i in range(0, len(self.users_saved_games_name(game_list))):
                print("\t{}".format(self.users_saved_games_name(game_list)[i]))
            return True

    def save_game(self) -> bool:
        # Based on the user name the saved games listed to separate saved games by name
        # Validation of saved game name, min 3 chars and have to differ from the existing names
        print("\tJáték mentése menü:")
        save_game_list = []
        try:
            save_game_list.extend(self.load_games())
        except json.decoder.JSONDecodeError:
            print("\tKorábbi mentések betöltése sikertelen! Valószínüleg nem voltak korábbi mentések!")
            pass
        user_input = ""
        while user_input != "n":
            user_input = input("\tKivánja menteni az aktuális játék állást?(y/n)")
            if user_input == "y":
                self.print_users_saved_games(save_game_list)
                save_game_name = input("\tAz új mentés neve:")
                if len(save_game_name) < 4:
                    print("\tLegalább 3 karakter hosszú nevet adjon meg a mentésnek!")
                    continue
                elif save_game_name in self.users_saved_games_name(save_game_list):
                    print("\tA megadott mentésnév már létezik, kérem adjon meg másikat!")
                    continue
                else:
                    save_game_list.append({"Player name": self.Player.name,
                                           "Game name": save_game_name,
                                           "Saved game data": self.to_json(),
                                           })
                    with open("gamesaves.json", "w", encoding="UTF-8") as saved_games_file:
                        saved_games_file.write(json.dumps(save_game_list, indent=4, ensure_ascii=False))
                    return True
            elif user_input == "n":
                return False
            else:
                print("\tNem megfelelő válasz!")
                continue

    def closing_round(self):
        #  Roasting cards and clearing hands at the end of each round
        self.Roasted_Cards.extend(copy.deepcopy(self.Player.player_cards))
        self.Roasted_Cards.extend(copy.deepcopy(self.Player.split_cards))
        self.Dealer.player_cards[0].representation = self.__Dealer_first_card_repr
        self.Roasted_Cards.extend(copy.deepcopy(self.Dealer.player_cards))
        self.Dealer.player_cards.clear()
        self.Player.player_cards.clear()
        self.Player.split_cards.clear()
        if self.__doubling_decision:  # in case of doubling need to set back the bets for the previous values
            self.Player.bet = self.Player.bet / 2
            self.Dealer.bet = self.Dealer.bet / 2
            self.__doubling_decision = False
        self.__insurance_decision = False
        self.__insurance_offer = False
        self.__splitting_decision = False
        self.__splitting_offer = False

    def print_player_hand_and_value(self, is_split=False):
        if not is_split:
            self.Player.print_cards_nicely(is_split_cards=False)
            print("{Playername} lapjainak értéke:{Playercardvalues}".format(
                Playername=self.Player.name,
                Playercardvalues=(self.sum_card_values(self.Player.player_cards))))
        elif is_split:
            self.Player.print_cards_nicely(is_split_cards=True)
            print("{Playername} lapjainak értéke:{Playercardvalues}".format(
                Playername=self.Player.name,
                Playercardvalues=(self.sum_card_values(self.Player.split_cards))))

    def start_round(self):
        self.Player.statistics.nu_of_played_rounds += 1
        # Popping 2 - 2 cards of the mixed deck to the Dealer and Player as starting cards
        self.Player.bank -= self.Player.bet
        self.Dealer.bank -= self.Dealer.bet
        print("Az Osztó bankja: {}$".format(self.Dealer.bank))
        for k in range(0, 2):
            self.Dealer.player_cards.append(self.mixed.pop(-1))
        for j in range(0, 2):
            self.Player.player_cards.append(self.mixed.pop(-1))
        # have to turn down the dealer's first card :) and save in a class variable the turned down card repr
        self.__Dealer_first_card_repr = self.Dealer.player_cards[0].representation
        self.Dealer.player_cards[0].representation = Card.card_representation_backside
        self.Dealer.print_cards_nicely(is_split_cards=False)
        print("\nA játék tétje: {}$".format(self.Player.bet + self.Dealer.bet))
        self.print_player_hand_and_value(is_split=False)
        print("{Playername} bankja: {bank}$".format(Playername=self.Player.name, bank=self.Player.bank))
        if self.player_round(self.Player.player_cards) and not self.__insurance_decision \
                and not self.check_blackjack(checked_cards=self.Player.player_cards, isPlayer=True):
            if self.__splitting_decision:
                if self.check_blackjack(checked_cards=self.Player.split_cards, isPlayer=True):
                    self.closing_round()
                    return
            print("Az osztó köre következik!")
            time.sleep(1)
            if self.check_blackjack(checked_cards=self.Dealer.player_cards, isPlayer=False):
                self.Dealer.player_cards[0].representation = self.__Dealer_first_card_repr
                self.Dealer.print_cards_nicely(is_split_cards=False)
                print("Az Osztónak BLACKJACK-je van! Az Osztó nyert!")
                self.Dealer.bank += self.Player.bet + self.Dealer.bet
                self.Player.statistics.nu_of_lost_rounds += 1
                self.Player.statistics.lost_money += self.Player.bet
                if self.__splitting_decision:
                    self.Dealer.bank += self.Player.bet + self.Dealer.bet
                self.closing_round()
                return
            else:
                self.dealer_turn()
                if not self.check_blackjack(checked_cards=self.Player.player_cards, isPlayer=True):
                    print("Az alap kézben lévő lapok eredménye:")
                    self.evaluate_final_cards(self.Player.player_cards)
                if self.__splitting_decision and not self.check_blackjack(
                        checked_cards=self.Player.split_cards, isPlayer=True):
                    print("A megosztott kézben lévő lapok eredménye:")
                    self.evaluate_final_cards(self.Player.split_cards)
        self.closing_round()
        return

    def player_round(self, playing_hand=None, is_splitted=False) -> bool:
        if playing_hand is None:
            playing_hand = []
        black_jack_bool = self.check_blackjack(checked_cards=playing_hand, isPlayer=True)
        if black_jack_bool:
            print("BLACKJACK-je van! Az Osztó fizet 3:2-höz arányban!")
            self.Player.bank += self.Player.bet * 1.5 + self.Dealer.bet
            self.Dealer.bank -= self.Dealer.bet * 0.5
            self.Player.statistics.won_money += self.Player.bet * 1.5 + self.Dealer.bet
            time.sleep(1)
            return False
        elif not black_jack_bool:
            user_input = ""
            while user_input != "2":
                if is_splitted:
                    if self.evaluate_first_cards(playing_hand, split=True):
                        return True
                elif not is_splitted:
                    if self.evaluate_first_cards(playing_hand, split=False):
                        return True
                if self.sum_card_values(playing_hand) > 21:
                    print("A lapjainak értéke meghaladja a 21-et, Ön vesztett!")
                    self.Dealer.bank += self.Player.bet + self.Dealer.bet
                    self.Player.statistics.nu_of_lost_rounds += 1
                    self.Player.statistics.lost_money += self.Player.bet
                    return False
                print("1 : Lap kérése\n2 : Megállás")
                user_input = input("Az Ön választása: ")
                if self.game_config.is_valid_number_in_range(user_input, 2):
                    user_input = int(user_input)
                    if user_input == 1:
                        playing_hand.append(self.mixed.pop(-1))
                        if is_splitted:
                            self.print_player_hand_and_value(is_split=True)
                            continue
                        elif not is_splitted:
                            self.print_player_hand_and_value(is_split=False)
                    if user_input == 2:
                        return True
                else:
                    print("Érvénytelen választási lehetőség!")
                    continue

    def modify_starting_bet(self):
        # Dealer bet always equals with player bet! Only modify for the rounds the bet
        print("Kezdőtét módosítása menü:")
        print("A jelenlegi játék kezdőtét: {}$".format(self.Player.bet))
        print("Az Ön bankja: {}$".format(self.Player.bank))
        user_input = ""
        while user_input != "n":
            user_input = input("Változtatni kíván a kezdőtéten?(y/n)")
            if user_input == "y":
                new_bet = input("Adja meg az új kezdőtétet: ")
                if self.game_config.is_valid_number_in_range(new_bet, int(self.Player.bank)):
                    new_bet = float(new_bet)
                    self.Player.bet = new_bet
                    self.Dealer.bet = new_bet
                    print("Az új beállított kezdőtét: {}$".format(self.Player.bet))
                    break
                elif not self.game_config.is_valid_number_in_range(new_bet, int(self.Player.bank)):
                    print("Nem megfelelő kezdőtét!")
                    continue
            elif user_input == "n":
                break
            else:
                print("Érvénytelen választás!")
                continue
        return

    Round_starting_user_options = {
        RoundStartingUserOptions.START_ROUND.value: start_round,
        RoundStartingUserOptions.MODIFY_STARTING_BET.value: modify_starting_bet,
        RoundStartingUserOptions.SAVE_GAME.value: save_game,
        RoundStartingUserOptions.EXIT_GAME.value: exit_game,
    }

    # noinspection PyArgumentList
    def new_round_selector(self):
        user_input = ""
        while user_input != "4":
            print("\nA kezdő tét: {}$".format(self.Player.bet))
            print("Az Ön bankja: {}$".format(self.Player.bank))
            print("Az Osztó bankja: {}$".format(self.Dealer.bank))
            print("Az osztáshoz rakja meg a tétet!\n")
            print("""1: Tét megadása és lapok osztása
2: Kezdőtét módosítása
3: Játék állás mentése
4: Kilépés""")
            user_input = input("Az Ön választása: ")
            if self.game_config.is_valid_number_in_range(user_input, 3):
                self.Round_starting_user_options[int(user_input)](self)
                return True
            elif int(user_input) == 4:
                self.exit_game()
                return False
            else:
                print("Kérem adjon meg érvényes menüszámot!")
                continue

    def insurance_method(self):
        # if player decides to make the insurance immediately need to turn up the dealer's first card
        # and check for blackjack, if dealer has blackjack insurance paid for the player and round ends
        self.__insurance_bet = 0.0
        if self.Dealer.player_cards[1].name == "A":
            user_input = ""
            while user_input != "n":
                print("Lehetősége van biztosítást kötni {}$ értékben!".format(self.Player.bet / 2))
                user_input = input("Kiván biztosítást kötni az Osztó BlackJack-je esetére?(y/n)")
                if user_input == "y":
                    self.__insurance_bet = self.Player.bet / 2
                    if self.check_blackjack(checked_cards=self.Dealer.player_cards, isPlayer=False):
                        self.Dealer.player_cards[0].representation = self.__Dealer_first_card_repr
                        print("Az osztó lapjai:")
                        self.Dealer.print_cards_nicely(is_split_cards=False)
                        print("Az osztónak BlackJack-je van, mivel biztosítást kötött rá visszanyerte a tétjét!")
                        self.Player.bank += self.__insurance_bet * 2
                        self.Dealer.bank += self.Dealer.bet
                        self.Player.statistics.won_money += self.__insurance_bet
                        self.__insurance_decision = True
                        return True
                    else:
                        print("Az osztó lapjai ellenőrizve...")
                        print("Az osztónak nincs BlackJack-je, elvesztette a biztosítását, de kör folytatódik Önnel!")
                        self.Dealer.bank += self.__insurance_bet
                        self.Player.bank -= self.__insurance_bet
                        self.Player.statistics.lost_money += self.__insurance_bet
                        self.print_player_hand_and_value(is_split=False)
                        self.__insurance_decision = False
                        return False
                elif user_input == "n":
                    return False
                else:
                    print("Nincs ilyen választási lehetősége!")
                    continue
            return False
        else:
            return False

    def doubling(self, doubled_hand=None, splitting=False):
        # doubling offered when the first two cards value less or equal to 11,
        # Player can only ask for ONE more card but the bet doubled automatically
        if doubled_hand is None:
            doubled_hand = []
        user_input = ""
        while user_input != "n":
            user_input = input("Duplázásra van lehetőség!Kíván élni vele? (y/n)")
            if user_input == "y":
                self.Player.bank -= self.Player.bet
                self.Dealer.bank -= self.Dealer.bet
                self.Player.bet += self.Player.bet
                self.Dealer.bet += self.Dealer.bet
                print("\nA játék tétje: {}$-ra nőtt!".format(self.Player.bet + self.Dealer.bet))
                doubled_hand.append(self.mixed.pop(-1))
                if splitting:
                    self.print_player_hand_and_value(is_split=True)
                elif not splitting:
                    self.print_player_hand_and_value(is_split=False)
                time.sleep(1)
                self.__doubling_decision = True
                return True
            elif user_input == "n":
                self.__doubling_decision = False
                return False
            else:
                print("Nem megfelelő választási lehetőség!")
                continue

    def splitting(self) -> bool:
        # splitting is offered when two of the card names are the same
        user_input = ""
        while user_input != "n":
            user_input = input("Megosztásra van lehetőség!Kiván élni vele?(y/n)")
            if user_input == "y":
                self.Player.bank -= self.Player.bet
                self.Dealer.bank -= self.Dealer.bet
                print("\nA megosztott játék tétje: {}$".format(self.Player.bet + self.Dealer.bet))
                print("A megosztott kézben lévő lapok:")
                self.Player.split_cards.append(self.Player.player_cards.pop(-1))
                self.Player.split_cards.append(self.mixed.pop(-1))
                self.print_player_hand_and_value(is_split=True)
                print("{Playername} bankja: {bank}$".format(Playername=self.Player.name, bank=self.Player.bank))
                self.player_round(self.Player.split_cards, is_splitted=True)
                self.Player.player_cards.append(self.mixed.pop(-1))
                print("A alap kézben lévő lapok:")
                self.print_player_hand_and_value(is_split=False)
                self.player_round(self.Player.player_cards, is_splitted=False)
                return True
            elif user_input == "n":
                return False
            else:
                print("Nincs ilyen választási lehetőség!")
                continue

    def evaluate_first_cards(self, player_hand=None, split=False) -> bool:
        if player_hand is None:
            player_hand = []
        if self.Dealer.player_cards[1].name == "A" and not self.__insurance_offer:
            # Insurance should be offered for the player only once
            self.__insurance_offer = True
            if self.insurance_method():
                return True
            else:
                return False
        elif (player_hand[0].name == player_hand[1].name) and (self.Player.bank >= self.Player.bet * 2) \
                and not self.__splitting_offer:
            # Splitting should be offered if the card names are the same
            # and the player has enough money for the additional bet
            self.__splitting_offer = True
            self.__splitting_decision = self.splitting()
            if self.__splitting_decision:
                return True
            else:
                return False
        elif self.sum_card_values(player_hand) <= 11 and (self.Player.bank >= self.Player.bet * 2):
            # Offer for the player if the card values less than 11
            # And the player have enough money to afford doubling
            if split:
                if self.doubling(player_hand, splitting=True):
                    return True
                else:
                    return False
            elif not split:
                if self.doubling(player_hand, splitting=False):
                    return True
                else:
                    return False
        else:
            return False

    def evaluate_final_cards(self, eval_hand=None):
        if eval_hand is None:
            print("Nem lehetséges a végső lapok kiértékelése!")
            pass
        elif self.sum_card_values(eval_hand) > 21:
            print("A lapjainak értéke meghaladja a 21-et, Ön vesztett!")
            self.Dealer.bank += self.Player.bet + self.Dealer.bet
            self.Player.statistics.nu_of_lost_rounds += 1
            self.Player.statistics.lost_money += self.Player.bet
            return
        elif self.sum_card_values(self.Dealer.player_cards) > 21:
            print("A Osztó lapjainak értéke meghaladja a 21-et, Ön nyert!")
            self.Player.bank += self.Player.bet + self.Dealer.bet
            self.Player.statistics.nu_of_won_rounds += 1
            self.Player.statistics.won_money += self.Player.bet
            return
        elif self.sum_card_values(eval_hand) == self.sum_card_values(self.Dealer.player_cards):
            print("A lapok értékei megegyeznek, visszakapják a tétjeiket!")
            self.Player.bank += self.Player.bet
            self.Dealer.bank += self.Dealer.bet
            return
        elif self.sum_card_values(eval_hand) > self.sum_card_values(self.Dealer.player_cards):
            print("A lapjai értéke magasabb mint az Osztóé, Ön nyert!")
            self.Player.bank += self.Player.bet + self.Dealer.bet
            self.Player.statistics.nu_of_won_rounds += 1
            self.Player.statistics.won_money += self.Player.bet
            return
        elif self.sum_card_values(eval_hand) < self.sum_card_values(self.Dealer.player_cards):
            print("Az Osztó lapjai értéke magasabb mint az Öné, vesztett!")
            self.Dealer.bank += self.Player.bet + self.Dealer.bet
            self.Player.statistics.nu_of_lost_rounds += 1
            self.Player.statistics.lost_money += self.Player.bet
            return

    def new_game_with_init(self):
        self.initialise_new_game()
        self.new_game()
        return

    def new_game(self):
        self.Player.name = self.game_config.player_name
        self.Player.statistics.user_name = self.Player.name
        self.Player.statistics.load_stats()
        while True:
            if self.Player.bank < self.game_config.starting_bet:
                print("Vesztett a bankkal szemben, a játéknak vége! Visszatérés a főmenübe!")
                self.Player.statistics.nu_of_complete_loses += 1
                break
            elif self.Dealer.bank < self.game_config.starting_bet:
                print("Ön nyert a bankkal szemben! Gratulálok, vége a játéknak! Visszatérés a főmenübe!")
                self.Player.statistics.nu_of_complete_wins += 1
                break
            elif self.Player.bank < self.Player.bet:
                print("A játék kezdőtétje magasabb mint az Ön pénze, módosítsa a kezdőtétet úgy,"
                      "hogy több legyen mint az alaptét: {}$".format(self.game_config.starting_bet))
                self.modify_starting_bet()
                continue
            elif self.Dealer.bank < self.Dealer.bet:
                print("A játék kezdőtétje magasabb mint az Osztó pénze, módosítsa a kezdőtétet úgy,"
                      "hogy több legyen mint az alaptét {}$ és kevesebb mint az Osztó bankja!"
                      .format(self.game_config.starting_bet))
                self.modify_starting_bet()
                continue
            elif len(self.mixed) <= 50:
                print("\n\tKártyák keverése!Kis türelmet kér az Osztó!")
                self.shuffle_five_decks()
                time.sleep(2)
                continue
            else:
                round_selector_bool = self.new_round_selector()
                if round_selector_bool:
                    continue
                elif not round_selector_bool:
                    break
        self.Player.statistics.save_stats()
        return

    def load_game_menu(self):
        print("\tJáték betöltése menü:")
        self.Player.name = self.game_config.player_name
        save_game_list = []
        try:
            save_game_list.extend(self.load_games())
        except json.decoder.JSONDecodeError:
            print("\tKorábbi mentések betöltése sikertelen! Valószínüleg nem voltak korábbi mentések!")
            print("\tÍgy játék betöltése sem lehetséges!")
            return
        filtered_saved_games_list = list(filter(lambda x: x["Player name"] == self.Player.name, save_game_list))
        loaded_filtered_list = self.print_users_saved_games(filtered_saved_games_list)
        if loaded_filtered_list:
            user_input = input("\tMelyik játékot kívánja folytatni?")
            lgi = None  # loaded game's list index
            for i in range(0, len(filtered_saved_games_list)):
                if user_input == filtered_saved_games_list[i]["Game name"]:
                    print("Megtaláltam a keresett mentést és betöltöttem!")
                    lgi = filtered_saved_games_list[i]
                    break
            if lgi is None:
                print("Nem található a megadott névvel mentés!")
                return
            elif lgi is not None:
                self.Player.bank = lgi["Saved game data"]["Player"]["Player bank"]
                self.Player.bet = lgi["Saved game data"]["Player"]["Player bet"]
                self.Dealer.name = "Osztó"
                self.Dealer.bank = lgi["Saved game data"]["Dealer"]["Dealer bank"]
                self.Dealer.bet = lgi["Saved game data"]["Dealer"]["Dealer bet"]
                json_mixed_deck = lgi["Saved game data"]["Mixed deck"]
                for i in range(0, len(json_mixed_deck)):
                    for key in self.deck.deck_dict.keys():
                        if key == json_mixed_deck[i]:
                            self.mixed.append(Card(
                                color=self.deck.deck_dict[key]["color"],
                                name=self.deck.deck_dict[key]["name"],
                                value=self.deck.deck_dict[key]["value"],
                                representation=self.deck.deck_dict[key]["representation"]))
                json_roasted_cards = lgi["Saved game data"]["Roasted cards"]
                for i in range(0, len(json_roasted_cards)):
                    for key in self.deck.deck_dict.keys():
                        if key == json_roasted_cards[i]:
                            self.Roasted_Cards.append(Card(
                                color=self.deck.deck_dict[key]["color"],
                                name=self.deck.deck_dict[key]["name"],
                                value=self.deck.deck_dict[key]["value"],
                                representation=self.deck.deck_dict[key]["representation"]))
                print(f'Kevert pakli lapjainak száma: {len(self.mixed)}')
                print(f'Égetett pakli lapjainak száma: {len(self.Roasted_Cards)}')
                self.new_game()
                filtered_saved_games_list.clear()
                return
        elif loaded_filtered_list is None:
            return

    def statistics(self):
        # Need to save: - nu of rounds in the game, - ratio of win or loss of the player
        # - nu of blackjacks of the player, all the money lost or win of user (minus is lost money)
        # play time of the player
        # display by player names and next the data like an "excel sheet" sort by number of won games
        print("Az eddigi játékok statisztikája játékos nevek szerint:")
        self.Player.statistics.user_name = self.Player.name
        self.Player.statistics.print_stats_sorted()
        return

    def quit_function(self):
        self.game_config.write_config_file()
        print("Ön kilépett és a játék beállítások mentésre került!")
        time.sleep(2)
        sys.exit(0)

    def welcomeScreen(self):
        print("Üdvözlöm a QuarantineJack Játékba {}!".format(self.Player.name))
        self.deck.load_cards_to_dict()
        self.deck.load_cards()
        self.Player.player_cards.append(self.deck.Cards[0])
        self.Player.player_cards.append(self.deck.Cards[25])
        self.Player.player_cards.append(self.deck.Cards[37])
        self.Player.player_cards.append(self.deck.Cards[49])
        self.Player.print_cards_nicely(is_split_cards=False)
        self.Player.player_cards.clear()

    def shuffle_five_decks(self):
        # Adding 5 decks to the mixed card list and shuffling the list items
        # Used extend IT IS NOT THE SAME AS APPEND!!!
        # Deep copy does not reflect any change to the original list
        # Copy takes any change in the original list to the copied one
        self.mixed.clear()
        for j in range(0, 5):
            self.mixed.extend(copy.deepcopy(self.deck.Cards))
        random.shuffle(self.mixed)

    Main_menu_user_options = {
        MainMenuOptions.NEW_GAME_WITH_INIT.value: new_game_with_init,
        MainMenuOptions.LOAD_GAME_MENU.value: load_game_menu,
        MainMenuOptions.CONFIG_SETTINGS.value: config.Config.settings,
        MainMenuOptions.STATISTICS.value: statistics,
        MainMenuOptions.QUIT_FUNCTION.value: quit_function,
    }

    # noinspection PyArgumentList,PyTypeChecker
    def main_menu(self):
        user_input = ""
        while user_input != "5":
            print("""
Fő menű:
1: Új játék indítása
2: Mentett játék betöltése
3: Beállítások
4: Statisztika megtekintése
5: Kilépés""")
            user_input = input("Válasszon a főmenüből parancsot:")
            if self.game_config.is_valid_number_in_range(user_input, 5):
                if int(user_input) == 3:
                    # noinspection PyArgumentList
                    self.Main_menu_user_options[int(user_input)](self.game_config)
                    continue
                else:
                    self.Main_menu_user_options[int(user_input)](self)
                    continue
            elif not self.game_config.is_valid_number_in_range(user_input, 5):
                print("Kérem adjon meg érvényes menüszámot!")
                continue


def main():
    while True:
        game = Game()
        game.game_config.set_game_config(game)
        game.welcomeScreen()
        game.Player.statistics.open_statistics_file()
        try:
            game.main_menu()
        except KeyboardInterrupt:
            game.quit_function()
            break
        # !!!Reminder: Only use class name for referencing when static function or static variable


if __name__ == '__main__':
    main()
