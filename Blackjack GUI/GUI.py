import pygame
from pygame.locals import *
import time
import main_menu
import main


class GameColors:
    black = (0, 0, 0)
    grey = (128, 128, 128)
    green_table = (53, 101, 77)
    white = (255, 255, 255)
    red = (255, 0, 0)
    green = (0, 128, 0)
    blue = (0, 0, 255)
    lime = (0, 255, 0)


class GameDisplay:
    display_height = 768
    display_width = 1024

    def __init__(self):
        self.game_display = pygame.display.set_mode((1024, 768))
        self.player_name: str = "Akárki"
        self.welcome_screen_run: bool = True
        self.menu_head_font = None
        self.game = main.Game()

    def init_game(self):
        self.menu_head_font = pygame.font.SysFont('impact', 40)
        self.set_game_icon_and_window_caption()
        self.game.game_config.set_game_config(game=self.game)
        print("parent_id:{pid}".format(pid=id(self.game.game_config.music_sound_level)))
        self.player_name = self.game.Player.name
        self.welcome_screen()

    def exit_function(self):
        print("parent_id:{pid}".format(pid=id(self.game.game_config.music_sound_level)))
        self.game.game_config.write_config_file()
        pygame.quit()
        quit()

    def welcome_screen(self):
        player_name = self.player_name
        menu = main_menu.MainMenu()
        # TODO valószínűsíthető probléma az, hogy itt van példányosítva a MainMenu gyerekosztály
        qj_pos_x = 135
        qj_pos_y = -100
        clock = pygame.time.Clock()
        while self.welcome_screen_run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.exit_function()
                elif event.type == pygame.MOUSEBUTTONUP:
                    menu.mouse_up = True
            if not menu.welcome_run:
                self.exit_function()
            self.game_display.fill(GameColors.white)
            self.drawText(
                text=f"Üdvözlöm a Quarantine Jack Játékba {player_name}!",
                font=self.menu_head_font,
                x=int(self.display_width / 2),
                y=40)
            self.game_display.blit(self.main_menu_back_image(), (qj_pos_x, qj_pos_y))
            menu.main_menu_buttons()
            pygame.display.update()
            clock.tick(30)

    @staticmethod
    def set_game_icon_and_window_caption():
        game_icon = pygame.image.load("icons//game_icon.png")
        pygame.display.set_icon(game_icon)
        pygame.display.set_caption('Quarantine Jack')

    @staticmethod
    def main_menu_back_image():
        # main menu background image, load, resize
        # return is an surface with the image
        qj_image = pygame.image.load('images//QJ_transparent.png')
        qj_rect = qj_image.get_rect()
        scale_ratio = 1.0
        qj_rect = (int(qj_rect[2] * scale_ratio), int(qj_rect[3] * scale_ratio))
        qj_image_scaled = pygame.transform.scale(qj_image, qj_rect)
        return qj_image_scaled

    def drawText(self, text: str = "", font_color=GameColors.black, font=None,
                 surface=None, x: int = 0, y: int = 0):
        if font is None:
            font = self.menu_head_font
        if text == "":
            text = "Nincs megadva szöveg a megjelenítéshez!"
        if surface is None:
            surface = self.game_display
        text_obj = font.render(text, 1, font_color)
        text_rect = text_obj.get_rect()
        text_rect.center = (x, y)
        surface.blit(text_obj, text_rect)


if __name__ == "__main__":
    pygame.init()
    start = GameDisplay()
    start.init_game()
