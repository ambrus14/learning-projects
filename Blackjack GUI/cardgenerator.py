import json

# french deck generator tool which generates the cards.json file
# cards.json is a nested dictionary, master dictionary 52 cards
# for every card you can access the details of the card like 'HeartsA' <--color+name
# for each card the secondary dictionary contains it's color, name, value and ascii art representation


card_colors = ["Hearts", "Tiles", "Clovers", "Pikes"]
card_name_list = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
card_value = {
    "A": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "10": 10,
    "J": 10,
    "Q": 10,
    "K": 10,
}

card_representation = {
    "Hearts": """ _______ \n|{card_name:^2}_ _ ♥|\n| ( v ) |\n|  \ /  |\n|   ˇ   |\n|♥____{card_name:^2}|""",
    "Tiles": """ _______ \n|{card_name:^2} ^  ♦|\n|  / \  |\n|  \ /  |\n|   ˇ   |\n|♦____{card_name:^2}|""",
    "Clovers": """ _______ \n|{card_name:^2} _  ♣|\n|  ( )  |\n| (_'_) |\n|   |   |\n|♣____{card_name:^2}|""",
    "Pikes": """ _______ \n|{card_name:^2} ^  ♠|\n|  /.\  |\n| (_._) |\n|   |   |\n|♠____{card_name:^2}|"""
}


deck_dict = {}

for color in card_colors:
    for name in card_value.keys():
        deck_dict[color + name] = {"color": color, "name": name, "value": card_value[name],
                                   "representation": card_representation[color].format(card_name=name)}

with open("E:\Code\Python\LogicBasedProgramming\BlackJack\cards.json", "w") as cards_file:
    json.dump(deck_dict, cards_file, indent=4)

"""
print(deck_dict["Tiles10"]["representation"])
print(deck_dict["Hearts10"]["representation"])
print(deck_dict["Pikes10"]["representation"])
print(deck_dict["Clovers10"]["representation"])

list = [{1:{2: 3}}]
with open("test.json", "w") as file:
    json.dump(list, file, indent=4)
list_2 = []
list_2.extend(list)
print(list_2)
with open("test.json", "r") as f:
    print(json.load(f))
"""