import GUI
import roundedfilledrectangle
import pygame
import config
import music
import time
from os import path


# subclass of game_display
class MainMenu(GUI.GameDisplay):

    def __init__(self):
        super().__init__()
        self.settings_run: bool = True
        self.mouse_up: bool = False
        self.sound_img_sx: int = 200
        self.sound_img_sy: int = 200
        self.img_trans: int = 128
        self.menu_head_font = pygame.font.SysFont('impact', 40)
        self.welcome_run: bool = True
        print("child_id:{}".format(id(self.game.game_config.music_sound_level)))

    def new_game_menu():
        pass

    def load_game_menu():
        pass

    @staticmethod
    def is_mouse_in_circle(x: int, y: int, r: int) -> bool:
        mouse_x = pygame.mouse.get_pos()[0]
        mouse_y = pygame.mouse.get_pos()[1]
        sqx = (mouse_x - x) ** 2
        sqy = (mouse_y - y) ** 2
        if (sqx + sqy) ** 0.5 < r:
            return True
        else:
            return False

    def click_in_circle(self, x: int, y: int, r: int) -> bool:
        click = pygame.mouse.get_pressed()
        if self.is_mouse_in_circle(x, y, r) and self.mouse_up:
            if click[0] == 1:
                self.mouse_up = False
                return True
            else:
                return False

    def music_sound_level_icons(self):
        vol_inc_posx = int(((self.display_width / 5) * 2) - self.sound_img_sx / 2)
        vol_dec_posx = int(((self.display_width / 5) * 4) - self.sound_img_sx / 2)
        vol_inc_img = pygame.transform.scale(pygame.image.load('images/volume+.png'),
                                             (self.sound_img_sx, self.sound_img_sy))
        vol_dec_img = pygame.transform.scale(pygame.image.load('images/volume-.png'),
                                             (self.sound_img_sx, self.sound_img_sy))
        vol_inc_rect = vol_inc_img.get_rect()
        vol_dec_rect = vol_inc_img.get_rect()
        vol_inc_rect.center = (vol_inc_posx, int(self.display_height / 2))
        vol_dec_rect.center = (vol_dec_posx, int(self.display_height / 2))

        vol_inc_trans = pygame.transform.scale(pygame.image.load('images/volume+trans.png'),
                                               (self.sound_img_sx, self.sound_img_sy))
        vol_dec_trans = pygame.transform.scale(pygame.image.load('images/volume-trans.png'),
                                               (self.sound_img_sx, self.sound_img_sy))
        self.game_display.blit(vol_inc_trans if self.is_mouse_in_circle(
            x=vol_inc_posx,
            y=int(self.display_height / 2),
            r=int(self.sound_img_sx / 2)) == True else vol_inc_img, vol_inc_rect)
        self.game_display.blit(vol_dec_trans if self.is_mouse_in_circle(
            x=vol_dec_posx,
            y=int(self.display_height / 2),
            r=int(self.sound_img_sx / 2)) == True else vol_dec_img, vol_dec_rect)
        if self.click_in_circle(
                x=vol_inc_posx,
                y=int(self.display_height / 2),
                r=int(self.sound_img_sx / 2)):
            music.volume_Increase()
            self.game.game_config.music_sound_level = int(music.pygame.mixer.music.get_volume() * 100)
            # print(id(self.game.game_config.music_sound_level))
            # print(self.game.game_config.music_sound_level)
            # TODO not working to save the sound level due to different memory space called here
        elif self.click_in_circle(
                x=vol_dec_posx,
                y=int(self.display_height / 2),
                r=int(self.sound_img_sx / 2)):
            music.volume_Decrease()
            self.game.game_config.music_sound_level = int(music.pygame.mixer.music.get_volume() * 100)
            # print(self.game.game_config.music_sound_level)
        elif pygame.mixer.music.get_volume() == 1:
            self.drawText(
                text="Elérte a maximális hangerőt!",
                x=int(self.display_width / 2),
                y=int(self.display_height / 3))
        elif pygame.mixer.music.get_volume() <= 0.2:
            self.drawText(
                text="Elérte a minimális hangerőt!",
                x=int(self.display_width / 2),
                y=int(self.display_height / 3))

    def music_sound_level_menu(self):
        back_b_x = int(self.display_width / 2)
        back_b_y = int(self.display_height * 0.8)
        back_button_r = 130
        while not self.click_in_circle(back_b_x, back_b_y, back_button_r):
            self.game_display.fill(GUI.GameColors.white)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.welcome_run = False
                    return
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.mouse_up = True
                self.drawText(
                    text="Hangerő beállítása menü",
                    font=self.menu_head_font,
                    x=int(self.display_width / 2),
                    y=40)
                self.drawText(
                    text="Az aktuális hangerő: {}%".format(music.actual_volume_level()),
                    x=int(self.display_width / 2),
                    y=120)
                pygame.draw.circle(self.game_display,
                                   GUI.GameColors.grey
                                   if self.is_mouse_in_circle(back_b_x, back_b_y,
                                                              back_button_r) == True else GUI.GameColors.black,
                                   (back_b_x, back_b_y),
                                   back_button_r)
                self.drawText(
                    text="Vissza a beállításokba",
                    font=pygame.font.SysFont('impact', 25),
                    x=back_b_x,
                    y=back_b_y,
                    font_color=GUI.GameColors.white)
                self.music_sound_level_icons()
                pygame.display.update()
        return

    def settings_menu(self):
        while self.settings_run:
            self.game_display.fill(GUI.GameColors.white)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.welcome_run = False
                    return
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.mouse_up = True
            if not self.welcome_run:
                break
            self.drawText(
                text="Beállítások menü",
                font=pygame.font.SysFont('impact', 40),
                surface=self.game_display,
                x=int(self.display_width / 2),
                y=40)
            self.settings_buttons()
            self.settings_side_images()
            pygame.display.update()
        self.settings_run = False
        return

    def settings_side_images(self):
        img_pos_x_left = 139
        img_pos_x_right = 801
        img_pos_y = 130
        img_spacing_y = 100
        chip_1 = pygame.image.load('images/casino_chips/chip_1.png')
        chip_5 = pygame.image.load('images/casino_chips/chip_5.png')
        chip_10 = pygame.image.load('images/casino_chips/chip_10.png')
        chip_50 = pygame.image.load('images/casino_chips/chip_50.png')
        chip_100 = pygame.image.load('images/casino_chips/chip_100.png')
        self.game_display.blit(chip_1, (img_pos_x_right, img_pos_y))
        self.game_display.blit(chip_1, (img_pos_x_left, img_pos_y))
        self.game_display.blit(chip_5, (img_pos_x_right, img_pos_y + img_spacing_y))
        self.game_display.blit(chip_5, (img_pos_x_left, img_pos_y + img_spacing_y))
        self.game_display.blit(chip_10, (img_pos_x_right, img_pos_y + img_spacing_y * 2))
        self.game_display.blit(chip_10, (img_pos_x_left, img_pos_y + img_spacing_y * 2))
        self.game_display.blit(chip_50, (img_pos_x_right, img_pos_y + img_spacing_y * 3))
        self.game_display.blit(chip_50, (img_pos_x_left, img_pos_y + img_spacing_y * 3))
        self.game_display.blit(chip_100, (img_pos_x_right, img_pos_y + img_spacing_y * 4))
        self.game_display.blit(chip_100, (img_pos_x_left, img_pos_y + img_spacing_y * 4))

    @staticmethod
    def music_button_info() -> str:
        music_status = music.is_playing_music()
        if path.exists('music/Casino_music.mp3'):
            if music_status:
                return 'Zene KI kapcsolása'
            else:
                return 'Zene BE kapcsolása'
        elif not path.exists('music/Casino_music.mp3'):
            return 'Nem található a zene fájl!'

    settings_button_texts = {
        0: 'Játékos név változtatása',
        1: 'Kezdőpénz (bank) változtatása',
        2: 'Kezdőtét változtatása',
        3: 'Hangerő beállítása',
        4: None,
        5: 'Aktuális beállítások mentése',
        6: 'Mentett beállítások betöltése',
        7: 'Gyári beállítások betöltése',
        8: 'Vissza a főmenübe'
    }

    def settings_buttons(self):
        """  change player name
         change starting money
         change base bet
         Adjust sound level
         Turn music on-off
         save actual settings
         load last saved settings
         load default settings
         back to main menu """
        buttonsizex = 300
        buttonsizey = 60
        buttonposx = self.display_width / 2 - buttonsizex / 2
        buttonposy = 80
        buttontextposx = self.display_width / 2
        buttontextposy = buttonposy + 29
        buttonspacing = 70

        inc_buttonposy = buttonposy
        for i in range(0, 9):
            roundedfilledrectangle.filledRoundedRect(surface=self.game_display,
                                                     rect=(buttonposx, inc_buttonposy, buttonsizex, buttonsizey),
                                                     color=GUI.GameColors.grey if self.is_mouse_in_rectangle(
                                                         buttonposx, buttonsizex, inc_buttonposy, buttonsizey) == True
                                                     else GUI.GameColors.black,
                                                     radius=1)
            # spacing between the buttons
            inc_buttonposy += buttonspacing

        for i in range(0, 9):
            self.drawText(
                text=self.settings_button_texts[i] if i != 4 else self.music_button_info(),
                x=buttontextposx,
                y=buttontextposy + buttonspacing * i,
                font_color=GUI.GameColors.white,
                font=pygame.font.SysFont('impact', 20))

        if self.click_in_rect(buttonposx, buttonsizex, buttonposy, buttonsizey):
            # config.set_player_name
            pass

        # change starting money
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing, buttonsizey):
            # config.set_starting_money
            pass

        #      change base bet
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 2, buttonsizey):
            # config.set_base_bet
            pass

        #      Adjust sound level
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 3, buttonsizey):
            self.settings_run = False
            self.music_sound_level_menu()
            self.settings_run = True

        #      Turn music on-off
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 4, buttonsizey):
            self.game.game_config.turn_music_on_off(click=True)

        #      save actual settings
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 5, buttonsizey):

            # config.save_actual_settings
            pass

        #      load last saved settings
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 6, buttonsizey):
            # config.load_saved_settings
            pass

        #      load default settings
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 7, buttonsizey):
            # config.load_default_settings
            pass

        #      back to main menu
        elif self.click_in_rect(buttonposx, buttonsizex, buttonposy + buttonspacing * 8, buttonsizey):
            self.settings_run = False
            return

    def stats_menu():
        pass

    @staticmethod
    def is_mouse_in_rectangle(posx, sizex, posy, sizey) -> bool:
        mouse = pygame.mouse.get_pos()
        if posx + sizex > mouse[0] > posx and posy + sizey > mouse[1] > posy:
            return True
        else:
            return False

    def click_in_rect(self, x, width, y, height) -> bool:
        click = pygame.mouse.get_pressed()
        if self.is_mouse_in_rectangle(x, width, y, height) and self.mouse_up:
            if click[0] == 1:
                self.mouse_up = False
                return True
            else:
                return False

    main_menu_buttons_text = {
        0: 'Új játék',
        1: 'Játék betöltése',
        2: 'Beállítások',
        3: 'Statisztika',
        4: 'Kilépés'
    }

    def main_menu_buttons(self):
        display = self.game_display
        buttonsizex = 140
        buttonsizey = 60
        buttonposx = 115
        buttonposy = 670
        buttontextposx = 185
        buttontextposy = buttonposy + 29
        buttonspacing = 160
        # new game button rounded rectangle
        # load game button
        # settings button
        # statistics button
        # quit button
        inc_buttonposx = buttonposx
        for i in range(0, 5):
            roundedfilledrectangle.filledRoundedRect(surface=display,
                                                     rect=(inc_buttonposx, buttonposy, buttonsizex, buttonsizey),
                                                     color=GUI.GameColors.grey
                                                     if self.is_mouse_in_rectangle(inc_buttonposx, buttonsizex,
                                                                                   buttonposy,
                                                                                   buttonsizey) == True else GUI.GameColors.black,
                                                     radius=0.8)
            # spacing between the buttons
            inc_buttonposx += buttonspacing
        # cycle to display correctly the button's text
        for i in range(0, 5):
            self.drawText(
                text=self.main_menu_buttons_text[i],
                font=pygame.font.SysFont('impact', 20),
                font_color=GUI.GameColors.white,
                x=buttontextposx + buttonspacing * i,
                y=buttontextposy
            )
        if self.click_in_rect(buttonposx, buttonsizex, buttonposy, buttonsizey):
            self.new_game_menu()
        elif self.click_in_rect(buttonposx + buttonspacing, buttonsizex, buttonposy, buttonsizey):
            self.load_game_menu()
        elif self.click_in_rect(buttonposx + buttonspacing * 2, buttonsizex, buttonposy, buttonsizey):
            self.settings_run = True
            self.settings_menu()
            return
        elif self.click_in_rect(buttonposx + buttonspacing * 3, buttonsizex, buttonposy, buttonsizey):
            self.stats_menu()
        elif self.click_in_rect(buttonposx + buttonspacing * 4, buttonsizex, buttonposy, buttonsizey):
            self.welcome_run = False
            return
