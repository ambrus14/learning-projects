import json
import typing


class Card:
    card_representation_backside = """ _______ 
| \ ~ / |
| }}:{{ |
| }}:{{ |
| }}:{{ |
| /_~_\ |"""

    def __init__(self, color=None, name=None, value=None, representation=None):
        self.color = color
        self.name = name
        self.value = value
        self.representation = representation


class Deck:
    def __init__(self):
        self.Cards: typing.List[Card] = list()
        self.deck_dict: typing.Dict[Card] = dict()

    def __len__(self):
        return len(self.Cards)

    def load_cards_to_dict(self):
        with open("cards.json", "r") as cards_file:
            self.deck_dict = json.load(cards_file)

    def load_cards(self):
        for keys in self.deck_dict.keys():
            self.Cards.append(Card(color=self.deck_dict[keys]["color"],
                                   name=self.deck_dict[keys]["name"],
                                   value=self.deck_dict[keys]["value"],
                                   representation=self.deck_dict[keys]["representation"]))


"""
def key_value_of_dictionary_printing(dict_1):
    for key, value in dict_1.items():
        print(key, value)
print(deck.Cards[0].color)
print(len(deck))
"""
