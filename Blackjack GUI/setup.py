import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], 'include_files': ['cardgenerator.py', 'cards.json',
                                                           'cards.py', 'config.ini', 'config.py', 'gamesaves.json',
                                                           'music.py', 'statistics.py']}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = 'console'

setup(name="Quarantine Jack",
      version="0.1",
      description="My Black Jack application!",
      options={"build_exe": build_exe_options},
      executables=[Executable("main.py", base=base)])
