import enum
import os
import os.path
from configparser import ConfigParser
from os import path

import music


class SettingMenuOptions(enum.Enum):
    SET_PLAYER_NAME = 1
    SET_STARTING_MONEY = 2
    SET_BASE_BET = 3
    SET_MUSIC_SOUND_LEVEL = 4
    TURN_MUSIC_ON_OFF = 5
    SAVE_ACTUAL_SETTINGS = 6
    LOAD_SAVED_SETTINGS = 7
    LOAD_DEFAULT_SETTINGS = 8

class MusicSoundLevelMenu(enum.Enum):
    INCREASE_VOLUME = 1
    DECREASE_VOLUME = 2
    BACK_TO_SETTINGS = 3


# noinspection PyArgumentList
class Config:
    config = ConfigParser()

    def __init__(self):
        self.player_name: str = ""
        self.starting_money: int = 0
        self.starting_bet: float = 0.0
        self.music_sound_level: int = 0
        self.music_enabled: bool = False

    @staticmethod
    def is_valid_number_in_range(str_number, in_range) -> bool:
        try:
            number = int(str_number)
            if number in range(1, in_range + 1):
                return True
            else:
                return False
        except ValueError:
            return False
        except TypeError:
            return False

    def read_config_file(self):
        Config.config.read('config.ini')
        self.player_name = str(Config.config['settings']['player_name'])
        self.starting_money = eval(Config.config['settings']['starting_money'])
        self.starting_bet = eval(Config.config['settings']['starting_bet'])
        self.music_sound_level = eval(Config.config['settings']['music_level'])
        self.music_enabled = eval(Config.config['settings']['music_enabled'])
        return
        # eval is used to cast the string for the correct data type

    def write_config_file(self):
        Config.config['settings'] = {
            'player_name': self.player_name,
            'starting_money': self.starting_money,
            'starting_bet': self.starting_bet,
            'music_level': self.music_sound_level,
            'music_enabled': self.music_enabled,
        }
        with open("config.ini", "w") as config_file:
            Config.config.write(config_file)

    def set_game_config(self, game):
        self.read_config_file()
        music.set_volume(self.music_sound_level)
        if self.music_enabled:
            music.play_music()
        game.Player.name = self.player_name

    def set_player_name(self):
        print("\n\t\tJátékos név változtatása menü:")
        print("\t\tA játékos neve: {}".format(self.player_name))
        user_input = ""
        while user_input != "n":
            user_input = input("\t\tMeg kívánja változtatni a játékos nevet? (y/n)")
            if user_input == "y":
                new_name = input("\t\tAdja meg az új játékos nevet:")
                if user_input == self.player_name:
                    print("\t\tUgyanazt a játékos nevet adta meg mint ami be volt állítva!")
                    continue
                if len(new_name) <= 3:
                    print("\t\tLegalább 3 karakter hosszú játékosnevet adjon meg!")
                    continue
                else:
                    Config.config['settings']['player_name'] = new_name
                    self.player_name = new_name
                    print("\t\tAz új játékosnév: {}".format(self.player_name))
                    return
            else:
                print("\t\tNincs ilyen választási lehetőség!")
                continue
        print("\t\tNem változtatta meg a játékos nevet!")
        return

    def set_starting_money(self):
        print("\n\t\tKezdő pénz (Bank) beállítása menü:")
        print("\t\tAz aktuális kezdőpénz:{}$".format(self.starting_money))
        user_input = ""
        while user_input != "n":
            user_input = input("\t\tMeg kívánja változtatni a kezdőpénzt?(y/n)")
            if user_input == "y":
                new_starting_money = input("\t\tAdja meg az új kezdőpénz mennyiségét:")
                if self.is_valid_number_in_range(new_starting_money, 10 ** 100):
                    self.starting_money = int(new_starting_money)
                    if (self.starting_money / 2) >= self.starting_bet:
                        print("\t\tAz új beállított kezdőpénz:{}$".format(self.starting_money))
                        return
                    else:
                        print("\t\tAz új beállított kezdőpénz legalább a duplája kell legyen a kezdőtétnek!")
                        continue
                elif not self.is_valid_number_in_range(new_starting_money, 10 ** 100):
                    print("\t\tAdjon meg érvényes egész számot az új kezdőpénznek ami kevesebb mint 10^100!")
                    continue
            else:
                print("\t\tÉrvényes választási lehetőséget adjon meg!")
                continue
        return

    def set_base_bet(self):
        print("\n\t\tAlaptét módosíása menü:")
        print("\t\tAz aktuális alaptét:{}$".format(self.starting_bet))
        user_input = ""
        while user_input != "n":
            user_input = input("\t\tMeg kívánja változtatni az alaptétet?(y/n)")
            if user_input == "y":
                new_base_bet = input("\t\tAdja meg az új alaptétet:")
                if self.is_valid_number_in_range(new_base_bet, 5 ** 99):
                    self.starting_bet = int(new_base_bet)
                    if self.starting_bet * 2 <= self.starting_money:
                        print("\t\tAz új beállított alaptét:{}$".format(self.starting_bet))
                        return
                    else:
                        print("\t\tAz új beállított alaptét legalább fele kell legyen a kezdőpénznek!")
                        continue
                elif not self.is_valid_number_in_range(new_base_bet, 5 ** 99):
                    print("\t\tAdjon meg érvényes egész számot az új alaptétnek ami kevesebb mint 5^99-en!")
                    continue
            else:
                print("\t\tÉrvényes választási lehetőséget adjon meg!")
                continue
        return

    def set_music_sound_level(self):
        print("\n\t\tHangerő beállítása menü:")
        music.actual_volume_level()
        print("\t\t1: Hangerő növelése! :)")
        print("\t\t2: Hangerő csökkentése :(")
        print("\t\t3 : Vissza a Beállítások menübe")
        while True:
            user_input = input("\t\tAz Ön választása:")
            if self.is_valid_number_in_range(user_input, 3):
                user_input = int(user_input)
                if user_input == MusicSoundLevelMenu.INCREASE_VOLUME.value:
                    music.musichandling("vol +")
                    self.music_sound_level = int(music.pygame.mixer.music.get_volume() * 100)
                    music.actual_volume_level()
                    continue
                elif user_input == MusicSoundLevelMenu.DECREASE_VOLUME.value:
                    music.musichandling("vol -")
                    self.music_sound_level = int(music.pygame.mixer.music.get_volume() * 100)
                    music.actual_volume_level()
                    continue
                elif user_input == MusicSoundLevelMenu.BACK_TO_SETTINGS.value:
                    print("\t\tBeállította a hangerőt!")
                    break
            elif not self.is_valid_number_in_range(user_input, 3):
                print("\t\tKérem adjon meg érvényes menüszámot!")
                continue
        return

    def turn_music_on_off(self, click):
        music_status = music.is_playing_music()
        user_input = click
        if music_status:
            while user_input:
                user_input = click
                if user_input:
                    music.musichandling("stop")
                    self.music_enabled = False
                    break
            return
        elif not music_status:
            while user_input:
                user_input = click
                if user_input and path.exists('music\Casino_music.mp3'):
                    music.musichandling("play")
                    self.music_enabled = True
                    break
                elif user_input and not path.exists('music/Casino music.mp3'):
                    print("\t\tNem kapcsolható be zene a fájl nélkül!")
                    self.music_enabled = False
                    break
            return

    def save_actual_settings(self):
        print("\n\t\tBeállítások mentése menü:")
        user_input = ""
        while user_input != "n":
            user_input = input("\t\tMenteni kívánja beállításokat?(y/n)")
            if user_input == "y":
                self.write_config_file()
                print("\t\tA beállítások mentése megtörtént!")
                return
            else:
                print("\t\tNincs ilyen választási lehetősége!")
                continue
        print("\t\tNem kerültek mentésre a beállítások!")
        return

    def load_saved_settings(self):
        print("\n\t\tBeállítások betöltése menü:")
        user_input = ""
        while user_input != "n":
            user_input = input("\t\tBe kívánja tölteni a mentett beállításokat?(y/n)")
            if user_input == "y":
                print("\t\tA mentett beállítások betöltésre kerültek!")
                # noinspection PyArgumentList
                self.set_game_config()
            else:
                print("\t\tNincs ilyen választási lehetősége!")
                continue
        print("\t\tNem kerültek betöltésre a mentett beállítások!")
        return

    def load_default_settings(self):
        print("\n\t\t Gyári beállítások betöltése menü:")
        while True:
            user_input = input("\t\t Valóban vissza akarja állítani a gyári beállításokat?(y/n)")
            if user_input == "y":
                print("\t\tGyári beállítások betöltése megtörtént!")
                Config.config.read('config.py.ini')
                self.player_name = str(Config.config['DEFAULT']['player_name'])
                self.starting_money = int(Config.config['DEFAULT']['starting_money'])
                self.starting_bet = float(Config.config['DEFAULT']['starting_bet'])
                self.music_sound_level = int(Config.config['DEFAULT']['music_level'])
                music.set_volume(self.music_sound_level)
                self.music_enabled = bool(Config.config['DEFAULT']['music_enabled'])
                if self.music_enabled:
                    music.play_music()
                return
            elif user_input == "n":
                print("\t\tA gyári beállításokat nem töltötte vissza!")
                break
            else:
                print("\t\tNincs ilyen választási lehetősége!")
                continue
        return

    Settings_Menu_user_options = {
        SettingMenuOptions.SET_PLAYER_NAME.value: set_player_name,
        SettingMenuOptions.SET_STARTING_MONEY.value: set_starting_money,
        SettingMenuOptions.SET_BASE_BET.value: set_base_bet,
        SettingMenuOptions.SET_MUSIC_SOUND_LEVEL.value: set_music_sound_level,
        SettingMenuOptions.TURN_MUSIC_ON_OFF.value: turn_music_on_off,
        SettingMenuOptions.SAVE_ACTUAL_SETTINGS.value: save_actual_settings,
        SettingMenuOptions.LOAD_SAVED_SETTINGS.value: load_saved_settings,
        SettingMenuOptions.LOAD_DEFAULT_SETTINGS.value: load_default_settings,
    }

    def settings(self):
        user_input = ""
        while user_input != "9":
            print("""\n\tBeállítások menü:
                1 : Játékos név változtatása
                2 : Kezdő pénz (bank) megváltoztatása
                3 : Alaptét megváltoztatása
                4 : Hangerő beállítása
                5 : Zene ki/be kapcsolása
                6 : Aktuális beállítások mentése
                7 : Utolsó mentett beállítások betöltése
                8 : Gyári beállítások betöltése
                9 : Vissza a főmenübe""")
            user_input = input("\tVálasszon a beállítások meüből:")
            if self.is_valid_number_in_range(user_input, 8):
                self.Settings_Menu_user_options[int(user_input)](self)
            elif not self.is_valid_number_in_range(user_input, 9):
                print("\tKérem adjon meg érvényes menüszámot!")
                continue
        print("\tVissza a főmenübe!")
        return