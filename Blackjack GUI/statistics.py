import json
import datetime


# noinspection PyTypeChecker
class statistics:
    def __init__(self):
        self.user_name: str = ""
        self.nu_of_played_rounds: int = 0
        self.nu_of_won_rounds: int = 0
        self.nu_of_lost_rounds: int = 0
        self.nu_of_blackjack: int = 0
        self.nu_of_complete_wins: int = 0
        self.nu_of_complete_loses: int = 0
        self.lost_money: float = 0.0
        self.won_money: float = 0.0
        self.play_time: str = ""
        self.stats_dict: dict = {}
        self.__starting_time: str = ""
        self.__ending_time: str = ""

    def start_time(self):
        self.__starting_time = datetime.datetime.now()

    def stop_time(self):
        self.__ending_time = datetime.datetime.now()
        saved_time = ""
        while True:
            try:
                saved_time = datetime.datetime.strptime(self.play_time, "%H:%M:%S")
                break
            except ValueError:
                self.play_time = "0:0:0"
                continue
        if self.__starting_time == "":
            self.__starting_time = datetime.datetime.now()
        self.play_time = saved_time + (self.__ending_time - self.__starting_time)
        self.play_time = f"{self.play_time.hour}:{self.play_time.minute}:{self.play_time.second}"

    def open_statistics_file(self):
        while True:
            try:
                with open("stats.json", "r", encoding="UTF-8") as stats_file:
                    self.stats_dict = json.load(stats_file)
                    break
            except FileNotFoundError:
                with open("stats.json", "x", ):
                    pass
                continue
            except json.decoder.JSONDecodeError:
                break

    def save_stats(self):
        self.stop_time()
        self.stats_dict[self.user_name]["number of won rounds"] = self.nu_of_won_rounds
        self.stats_dict[self.user_name]["number of lost rounds"] = self.nu_of_lost_rounds
        self.stats_dict[self.user_name]["number of played rounds"] = self.nu_of_played_rounds
        self.stats_dict[self.user_name]["number of blackjack"] = self.nu_of_blackjack
        self.stats_dict[self.user_name]["number of complete wins"] = self.nu_of_complete_wins
        self.stats_dict[self.user_name]["number of complete loses"] = self.nu_of_complete_loses
        self.stats_dict[self.user_name]["amount of won money"] = self.won_money
        self.stats_dict[self.user_name]["amount of lost money"] = self.lost_money
        self.stats_dict[self.user_name]["played time"] = self.play_time
        with open("stats.json", "w", encoding="UTF-8") as stat_file:
            stat_file.write(json.dumps(self.stats_dict, indent=4, ensure_ascii=False))

    def load_stats(self):
        self.open_statistics_file()
        if self.user_name not in self.stats_dict.keys():
            self.stats_dict[self.user_name] = {
                "number of won rounds": 0,
                "number of lost rounds": 0,
                "number of played rounds": 0,
                "number of blackjack": 0,
                "number of complete wins": 0,
                "number of complete loses": 0,
                "amount of won money": 0.0,
                "amount of lost money": 0.0,
                "played time": "0:0:0"}
        else:
            self.nu_of_won_rounds = self.stats_dict[self.user_name]["number of won rounds"]
            self.nu_of_lost_rounds = self.stats_dict[self.user_name]["number of lost rounds"]
            self.nu_of_played_rounds = self.stats_dict[self.user_name]["number of played rounds"]
            self.nu_of_blackjack = self.stats_dict[self.user_name]["number of blackjack"]
            self.nu_of_complete_wins = self.stats_dict[self.user_name]["number of complete wins"]
            self.nu_of_complete_loses = self.stats_dict[self.user_name]["number of complete loses"]
            self.lost_money = self.stats_dict[self.user_name]["amount of lost money"]
            self.won_money = self.stats_dict[self.user_name]["amount of won money"]
            self.play_time = self.stats_dict[self.user_name]["played time"]
        self.start_time()

    def win_lost_ratio(self, used_list: list = None, list_index: int = 0) -> float:
        if used_list is None:
            used_list = []
        return self.ratio_calculation(win=used_list[list_index][1]["number of won rounds"],
                                      lost=used_list[list_index][1]["number of lost rounds"])

    def win_lost_money_ratio(self, used_list: list = None, list_index: int = 0) -> float:
        if used_list is None:
            used_list = []
        return self.ratio_calculation(win=used_list[list_index][1]["amount of won money"],
                                      lost=used_list[list_index][1]["amount of lost money"])

    def win_lost_complete_ratio(self, used_list: list = None, list_index: int = 0) -> float:
        if used_list is None:
            used_list = []
        return self.ratio_calculation(win=used_list[list_index][1]["number of complete wins"],
                                      lost=used_list[list_index][1]["number of complete loses"])

    @staticmethod
    def ratio_calculation(win=0, lost=0) -> float:
        try:
            win_lost_ratio = win / lost
        except ZeroDivisionError:
            return 0.0
        return round(win_lost_ratio * 100, 2)

    def print_stats_sorted(self):
        # print statistics sorted by the number of played rounds
        if not self.stats_dict:
            self.load_stats()
        print("Játékos neve\tJátszott körök száma\tNyerési aránya\tBlackjackjei száma"
              "\tPénz nyerési aránya\tKomplett körök aránya\tJátékkal töltött ideje")
        sorted_stats_dict = sorted(self.stats_dict.items(),  key=lambda x: x[1]["number of played rounds"], reverse=True)
        for i in range(0, len(sorted_stats_dict)):
            print(f'{sorted_stats_dict[i][0]}\t\t{sorted_stats_dict[i][1]["number of played rounds"]}'
                  f'\t\t\t{self.win_lost_ratio(used_list=sorted_stats_dict, list_index=i)}%'
                  f'\t\t{sorted_stats_dict[i][1]["number of blackjack"]}'
                  f'\t\t\t{self.win_lost_money_ratio(used_list=sorted_stats_dict, list_index=i)}%'
                  f'\t\t\t{self.win_lost_complete_ratio(used_list=sorted_stats_dict, list_index=i)}%'
                  f'\t\t\t{sorted_stats_dict[i][1]["played time"]}')
